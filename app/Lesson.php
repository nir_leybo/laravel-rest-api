<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Tag;

class Lesson extends Model
{
    protected $fillable = ['title', 'body' ];

    protected $hidden = ['created_at', 'updated_at'];

    

    public function tags()
    {
    	return $this->belongsToMany(Tag::class);
    }
}
 