<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Lesson;
use App\Acme\Transformers\LessonsTransformer;

class LessonsController extends ApiController
{
    protected $lessonsTransformer;

    public function __construct(LessonsTransformer $lessonsTransformer)
    {
        $this->lessonsTransformer = $lessonsTransformer;

        // $this->beforeFilter('auth.basic');
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() 
    {
        //ex: http://localhost:8000/api/lessons?limit=2&page=2
        $limit = request()->limit ?: 3;

        $lessons =  Lesson::paginate($limit);

        // dd($lessons); // return pageinatot obj

        return $this->respondWithPagination($lessons, [
            'data' => $this->lessonsTransformer->transformCollection($lessons->all()) 
        ]);
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        if ( !request()->title or !request()->body ) 
        {
            return $this->setStatusCode(422)
                        ->respondWithError('Parameters failed validation for a lessons.');
        }

        $title = request()->title;
        $body = request()->body;

        Lesson::create([

            'title' => $title,
            'body' => $body

        ]);

        return $this->respondCreated('Lesson successfully created');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $lesson = Lesson::find($id);

        if (!$lesson) {

            return $this->respondNotFound('Lesson does not exist');
        }

        return $this->respond([

            'data' => $this->lessonsTransformer->transform($lesson)

        ]);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

}
