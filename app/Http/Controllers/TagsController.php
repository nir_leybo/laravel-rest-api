<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Tag;
use App\Lesson;
use App\Acme\Transformers\TagsTransformer;
use Illuminate\Database\Eloquent\ModelNotFoundException as ModelNotFoundException;

class TagsController extends ApiController
{
    protected $tagsTransformer;

    public function __construct(TagsTransformer $tagsTransformer)
    {
        $this->tagsTransformer = $tagsTransformer;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($lessonId = null)
    {
        $tags = $this->getTags($lessonId);

        return $this->respond([

            'data' => $this->tagsTransformer->transformCollection($tags->all())

        ]);
    }



    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $tag = Tag::find($id);

        if (!$tag) {

            return $this->respondNotFound('Tag does not exist');
        }

        return $this->respond([

            'data' => $this->tagsTransformer->transform($tag)

        ]);


    }


    private function getTags($lessonId)
    {
        // $tags = $lessonId ? Lesson::findOrFail($lessonId)->tags : Tag::all();
        
        $tags = collect(new Tag);// Empty collection.
        try
        {
            if ($lessonId) {
               $tags = Lesson::findOrFail($lessonId)->tags;
            }
        }
        catch (ModelNotFoundException $e) { }

        return $tags;
    }

}
