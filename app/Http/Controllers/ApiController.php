<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response as HeaderStatusCode;
use Illuminate\Pagination\LengthAwarePaginator as Paginator;

class ApiController extends Controller
{
    protected $statusCode = 200;

    public function getStatusCode(){
    	return $this->statusCode;
    }

    public function setStatusCode($statusCode){
    	$this->statusCode = $statusCode;
    	return $this;
    }


   /********* Error **********/
    public function respondNotFound($message = 'Not Found')
    {
       return $this->setStatusCode(HeaderStatusCode::HTTP_NOT_FOUND)->respondWithError($message);
    }

    public function respondInternalError($message = 'Internal Error')
    {
       return $this->setStatusCode(HeaderStatusCode::HTTP_INTERNAL_SERVER_ERROR)->respondWithError($message);
    }
    /********* End Error **********/



    public function respondCreated($message = 'Successfully created')
    {
       return $this->setStatusCode(201)->respond($message);
    }

    public function respond($data, $headers = [])
    {
    	return response()->json($data, $this->getStatusCode(), $headers);
    }

    public function respondWithError($message)
    {
       return $this->respond([

            'error' => [
                'message' => $message,
                'status_code' => $this->getStatusCode()
            ]

        ]);
    }

    public function respondWithPagination(Paginator $items, $data) 
    {
        $data = array_merge($data, [

            'paginator' => [

                'total_count' => $items->total(),
                'total_page' => ceil($items->total() / $items->perPage()),
                'current_page' => $items->currentPage(),
                'limit' => ceil($items->perPage())
            ]

        ]);

        return $this->respond($data);
    }

}
