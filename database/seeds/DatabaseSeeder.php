<?php

use Illuminate\Database\Seeder;
use App\Lesson;
use App\Tag;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	Lesson::truncate();
        Tag::truncate();
        DB::table('lesson_tag')->truncate();

    	Eloquent::unguard();

        $this->call(LessonsTableSeeder::class);
        $this->call(TagsTableSeeder::class);
        $this->call(LessonTagTableSeeder::class);
    }
}
